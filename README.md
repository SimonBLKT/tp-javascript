# TP JAvaScript

Mes Tp en JavaScript
> * Auteur : Simon Blanckaert

## Sommaire

  - Introduction
       - [HellWorld](Introduction/HelloWorld_JS/js/js/index.html)
   - Variable et operateur
     - [IMC](variable_operateur/IMC/index.html)
     - [Surface](variable_operateur/Surface/index.html)
     - [Temperature](variable_operateur/Temperature/index.html)
  
  - Structure de controle
    - [Calcul factorielle](Structure_de_controle/Calcul_factorielle/index.html)
    - [Euros/Dollar](Structure_de_controle/Euro_Dollar/index.html)
    - [Syracuse](Structure_de_controle/syracuse/index.html)
    - [Fiabonnaci](Structure_de_controle/Fiabonacci/index.html)
    - [Nombre Triple](Structure_de_controle/Nombre_triple/index.html)
    - [Table](Structure_de_controle/Table/index.html)