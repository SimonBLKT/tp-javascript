let poids = 100 ;//poids en KG
let taille = 160 ;       //taille en cm
let IMC = undefined;//IMC

IMC = poids / ((taille * taille) * 10e-5)//calcul IMC

console.log("Calcul de l'IMC :"); //affichage imc
console.log("taille : " + taille + "cm");
console.log("poids : " + poids + "kg");
console.log("IMC = " + IMC.toFixed(1));