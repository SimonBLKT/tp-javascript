//Crétion de l'objet et de ses proprietés
let objPatient =
{
    nom: "Dupond",
    prenom: "Jean",
    age: 30,
    sexe: "masculin",
    taille: 180,
    poids: 85,

    decrire: function () {
        let description;
        description = this["prenom"] + " " + this["nom"] + " est agé de " + this["age"] + " ans " + " de sexe " + this["sexe"] + " mesure " + this["taille"] + " cm " + " et pèse " + this["poids"] + " kg ";

        return description;
    },




    //fonction corpulence

    definir_corpulence: function () {

        let IMC;
        let taille = this.taille;
        let poids = this.poids;
        let interpretation;

        // Fonction Calculer
        function calculer_IMC() {


            IMC = poids / ((taille * taille) * 10e-5);
            IMC = IMC.toFixed(2);

        }

        //Fonctions interpreter
        function interpreter_IMC() {


            if (IMC < 16.5) {
                interpretation = "dénutrition";
            }
            else if ((IMC > 16.5 && IMC < 18.5)) {
                interpretation = "maigreur";
            }
            else if ((IMC > 18.5) && (IMC < 25)) {
                interpretation = "corpulence normale";
            }
            else if ((IMC > 25) && (IMC < 30)) {
                interpretation = "surpoids";
            }
            else if ((IMC > 30) && (IMC < 35)) {
                interpretation = "obésité modérée";
            }
            else if ((IMC > 35) && (IMC < 40)) {
                interpretation = "obésité sévère";
            }
            else if (IMC > 40) {
                interpretation = "obésité morbide";
            }

        }


        //Appel des fonctions internes
        calculer_IMC();
        interpreter_IMC();


        return "L'IMC du patient " + IMC + " Le patient est donc en situations de  " + interpretation;


    }

};
//Affichage
console.log(objPatient.decrire());
console.log(objPatient.definir_corpulence());
