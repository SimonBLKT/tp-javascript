//creation de la fonction constructeur
function patient(prmNom, prmPrenom, prmAge, prmTaille, prmPoids, prmSexe) {
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.taille = prmTaille;
    this.poids = prmPoids;
    this.sexe = prmSexe;
    this.decrire = function () {
        let description;
        let sexe = this.sexe

        if (sexe == "Masculin") { // Si le sexe est masculin
            description = "Le patient " + this.prenom + " " + this.nom + " est agé de " + this.age + " ans " + "de sexe " + this.sexe + ". Il mesure " + this.taille + "m et pèse " + this.poids + "kg";
        }
        else { //si le sexe est féminin
            description = "La patiente " + this.prenom + " " + this.nom + " est agée de " + this.age + " ans " + "de sexe " + this.sexe + ". Elle mesure " + this.taille + "m et pèse " + this.poids + "kg";
        }

        return description;
    }


    this.definir_corpulence = function () {


        let IMC;
        let taille = this.taille;
        let poids = this.poids;
        let sexe = this.sexe;
        let interpretation;





        // Fonction Calculer
        function calculer_IMC() {


            IMC = poids / ((taille * taille) * 10e-5);
            IMC = IMC.toFixed(2);

        }


        //Fonctions interpreter
        function interpreter_IMC() {
            if (sexe == "Masculin") {    // Si le sexe est Masculin

                if (IMC < 16.5) {
                    interpretation = "dénutrition";
                }
                else if ((IMC > 16.5 && IMC < 18.5)) {
                    interpretation = "maigreur";
                }
                else if ((IMC > 18.5) && (IMC < 25)) {
                    interpretation = "corpulence normale";
                }
                else if ((IMC > 25) && (IMC < 30)) {
                    interpretation = "surpoids";
                }
                else if ((IMC > 30) && (IMC < 35)) {
                    interpretation = "obésité modérée";
                }
                else if ((IMC > 35) && (IMC < 40)) {
                    interpretation = "obésité sévère";
                }
                else if (IMC > 40) {
                    interpretation = "obésité morbide";
                }
            }


            else { // Sinon

                if (IMC < 16.5) {
                    interpretation = "dénutrition";
                }
                else if ((IMC > 16.5 && IMC < 18.5)) {
                    interpretation = "maigreur";
                }
                else if ((IMC > 18.5) && (IMC < 25)) {
                    interpretation = "corpulence normale";
                }
                else if ((IMC > 25) && (IMC < 30)) {
                    interpretation = "surpoids";
                }
                else if ((IMC > 30) && (IMC < 35)) {
                    interpretation = "obésité modérée";
                }
                else if ((IMC > 35) && (IMC < 40)) {
                    interpretation = "obésité sévère";
                }
                else if (IMC > 40) {
                    interpretation = "obésité morbide";
                }
            }
        }



        //Appel des fonctions internes
        calculer_IMC();
        interpreter_IMC();
        return "L'IMC du patient " + IMC + " Le patient est donc en situations de  " + interpretation;;
    };
}




//creation des objets
let objPatient1 = new patient("Dupond", "Jean", 30, 180, 85, "masculin");
let objPatient2 = new patient("Moulin", "Isabelle", 46, 158, 74, "féminin");
let objPatient3 = new patient("Martin", "Eric", 42, 165, 90, "masculin");


//affichage des objets

console.log(objPatient1.decrire());
console.log(objPatient1.definir_corpulence());
console.log(objPatient2.decrire());
console.log(objPatient2.definir_corpulence());
console.log(objPatient3.decrire());
console.log(objPatient3.definir_corpulence());
