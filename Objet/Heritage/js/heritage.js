function Personne(prmNom, prmPrenom, prmAge, prmSexe) { // constructeur Personne 
    this.nom = prmNom;  // nom de la personne 
    this.prenom = prmPrenom;    // prénom de la personne 
    this.age = prmAge;  // age de la personne 
    this.sexe = prmSexe;    // sexe de la personne 
}

Personne.prototype.decrire = function() {   // description de la personne 
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return description;
}

function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {   // constructeur Professeur
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}

Professeur.prototype = Object.create(Personne.prototype);  
Professeur.prototype.constructor = Professeur;  
Professeur.prototype.decrire_plus = function() {    
    let description;
    let prefixe;
    if(this.sexe == 'M') {
        prefixe = 'Mr';
    } else {
        prefixe = 'Mme';
    }
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

function Eleve(prmNom, prmPrenom, prmAge, prmSexe) {    // constructeur Eleve 
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
}

Eleve.prototype = Object.create(Personne.prototype);   //constructeur Personne()
Eleve.prototype.constructor = Eleve;  

Eleve.prototype.decrire_plus = function() {    // constructeur Professeur()
    let description;
    let prefixe;
    if(this.sexe == 'M') {
        prefixe = 'un';
    } else {
        prefixe = 'une';
    }
    description = this.prenom + " " + this.nom + " est "+ prefixe + " élève de SNIR1";
    return description;
}

// Affichage professeur
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());

// Affichage élève 
let objEleve1 = new Eleve('Dutillieul', 'Dorian', 18, 'M');
console.log(objEleve1.decrire());
console.log(objEleve1.decrire_plus());
