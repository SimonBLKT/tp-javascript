// Déclarations de variables
let poids = 100;
let IMC;
let Interp_IMC;


function decrire_corpulence(prmTaille, prmPoids) {
    let valIMC;
    let interpretation = "";

    function calculerIMC(prmTaille, prmPoids) {
        valIMC = poids / ((taille * taille) * 10e-5);
        valIMC = valIMC.toFixed(1);
    }

    function interpreterIMC() {

        if (valIMC < 16.5) {
            interpretation = "dénutrition"
        }
        else if ((valIMC > 16.5 && valIMC < 18.5)) {
            interpretation = "maigreur"
        }
        else if ((valIMC > 18.5) && (valIMC < 25)) {
            interpretation = "corpulence normale"
        }
        else if ((valIMC > 25) && (valIMC < 30)) {
            interpretation = "surpoids"
        }
        else if ((valIMC > 30) && (valIMC < 35)) {
            interpretation = "obésité modérée"
        }
        else if ((valIMC > 35) && (valIMC < 40)) {
            interpretation = "obésité sévère"
        }
        else {
            interpretation = "obésité morbide"
        }
    }

    calculerIMC();
    interpreterIMC();

    return "Votre IMC est égal à " + valIMC + " : vous êtes en état de " + interpretation
}


console.log("Calcul de l'IMC :");
console.log("taille : " + taille + "cm");
console.log("poids : " + poids + "kg");


console.log(decrire_corpulence(taille, poids));