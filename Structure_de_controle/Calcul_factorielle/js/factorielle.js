//Variables
//Nombre de facteur n
let n;
//Boucle affichage
let i;
//Valeur max du vecteur
let factor = 10;
//Resultat
let result = 0;
//Message 
let msg;

//Programme
//Si le nombre est strictement superieur a 1
if(factor>1){
    //affiche la premiere ligne
    console.log("1! = 1");
    //Pour chaque valeur du facteur
    for(n=2;n<factor;n++){
        //On vide le message préparé et on remplace par celui de base
        msg = n+"! = 1";
        //reinitalisation resultat
        result = 1;
        //Pour chaque partie du calcul
        for(i=2;i<=n;i++){
            //On ajoute au message
            msg += " x "+i;
            //calcul resultat
            result = result*i;
        }
        //fin message
        msg += " = "+result;
        //Affichage
        console.log(msg);
    }
}
