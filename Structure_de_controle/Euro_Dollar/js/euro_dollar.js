// Fichier conversion.js
// programme qui permet de convertir des dollars en euros
// Thomas Alexandre 08/04/2021

let i = 1;                                                   // nombre d'euros
let message;                                                 // message
let resultat = undefined;                                    // resultat en dollars
// Tant que resultat <= 16384
do {
    
resultat = i *1.65; // Calcul
message = i +" euro(s) = " +resultat.toFixed(2) +" dollar(s)" // Message affiché 
i = i*2;                                                      // incrementation de l'euro *2
console.log(message);                                         // Affichage du message
} while (i <= 16384);
